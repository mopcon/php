FROM php:7.2

RUN apt-get update && apt-get install -y --no-install-recommends libssl-dev=1.1.1d-0+deb10u3 \
    zlib1g-dev=1:1.2.11.dfsg-1 \
    libpng-dev=1.6.36-6 \
    libjpeg-dev=1:1.5.2-2 \
    libfreetype6-dev=2.9.1-3+deb10u1 \
    && apt-get autoremove -y \
    && apt-get autoclean \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*
RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
  && docker-php-ext-install gd
